class PagesController < ApplicationController
  def about
    @heading = 'About'
    @text1 = 'Simple blog created with using Ruby2.6.3/Rails5.2.3/Bootstrap4.'
    @text2 = 'Implemented: Post add/edit/delete features; authentication (name:admin password:admin).'
  end
end
